
# Changelog

Les changements principaux sont documentés dans ce fichier.


## [Unreleased]
### Added

### Changed

### Fixed

### Security


## [1.4.1] - 2018-02-05
### Added
- Documentation de l'export/import des données depuis SOLIS
- Documentation d'installation complétée avec la liste des dépendances PHP

### Fixed
- Si une levée d'ambiguïté (homonymie) en utilisant l'année de naissance renvoyait **plusieurs** réponses, une réponse de 
type 'Personne inconnue' était émise alors qu'il faut dans ce cas renvoyer une réponse 'Recherche ambigüe'.  


## [1.4.0] - 2017-09-22
### Added
- Première version stable 


## [1.3.1] - 2017-04-27
### Added
- Premier dépôt de la version en cours de refactoring. Cette version n'est pas une version stable utilisable en production 
