# Pourquoi l’application "Départements & Notaires" ?
   Pour apporter une réponse en temps réél aux études notariales chargées d’une 
succession et s'interrogant sur l'existence éventuelle d'une créance du Département au titre de l'aide sociale.

___


# Fonctionnement général de "Départements & Notaires"
**"Départements & Notaires"** est un extranet mis à disposition des études notariales du Rhône.<br/>
Chaque étude notariale du Rhône dispose d’un compte de connexion (identifiant professionnel, nom, adresse email).<br/>
Ce compte permet aux études notariales chargées d'une succession d'interroger le Département sur l'existence éventuelle d'une créance au titre de l'aide sociale.<br/>
Pour chaque recherche, les études notariales saisissent sur les informations d’état civil du défunt dont ils gèrent la succession.<br/>
Ces informations sont instantanément croisées avec les données issues de l’application de gestion des prestations 
d’aides sociales du Département.<br/> 
Instantannément, les études reçoivent une réponse du Département, accompagné d’un email contenant le courrier de réponse officiel.
___

# Quelles informations extraire de l'application tiers ?
Quotidiennement, la base de données dédiée à l’application "Département et Notaires"
doit être alimentée des informations issues de l'application tiers de gestion des prestations d’aides sociales.
<br/><br/>

Ces informations, stockées dans la table 'Individus', correspondent aux décisions d'intervention : 
<br/><br/>

-	récupérables : décisions d'ASG (Aide Sociale Générale), codées « 1SEXTREC » 
-	non récupérables : décisions d'APA : Allocation Personnalisée Autonomie et Décisions de PCH : Prestation de Compensation du Handicap), codées « 1SEXTNONR » 
<br/><br/>


-> A ces décisions d'intervention sont rattachés :

<br/>
-	l’individu 
    *	nom
    *	nom de naissance
    *	prénom
    *	date de naissance 
    *	civilité
    *	domicile de secours
<br/><br/>
-	le gestionnaire territorial du dossier
    *  	nom
    *	numéro de téléphone
    *	adresse
<br/><br/>

Une fois déposées dans la base de données, ces listes de décisions sont utilisées 
par le moteur de recherche. Les informations relatives à l'individu sont ainsi comparées aux informations d’état 
civil renseignées par les notaires.
___


# Quelles informations saisir pour rechercher un individu ?
Pour rechercher un individu, les études notariales renseignent les critères suivants : 

*	Obligatoirement :
    *	le nom d’usage, le(s) prénom(s), l’année de naissance ;
    *	la date de délivrance de l’acte de décès, la date et le lieu de décès
<br/><br/>
*	Facultativement : 
    *	le nom de naissance, le jour et le mois de naissance

Les informations relatives à l’acte de décès sont uniquement déclaratives. 
Elles permettent de s’assurer que l’étude notariale est bien en possession d’un acte de décès légitimant sa recherche. <br/>
Celles relatives à l’état civil sont en revanche exploitées par le moteur de recherche.

___


# Comment fonctionne le moteur de recherche ?

## Technologie
Le moteur de recherche est une requete SQL croisant les informations d'état civil renseignées par les notaires 
à celles issues de l'application tiers.

## Principe du moteur de recherche
Le principe du moteur est de tester différentes combinaisons de recherche comparant 
les informations d'état civil renseignées par les notaires à celles issues de l'application tiers.

##	Les variables utilisées
Le moteur de recherche reçoit en entrée les informations d'état civil fournies par les études notariales 
dans le cadre d'une recherche en succession.<br/>
Les données en entrée sont le nom d'usage ou le nom d'état civil, les prénoms et la date de naissance de la personne.<br/>
Ces données sont utilisées brutes dans toute la correspondance (courrier, message, listing de recherche) 
mais sont standardisées durant la recherche.<br/>
Cette standardisation correspond au retrait des accents, caractères spéciaux ou espace.<br/>
Cette action permet de comparer des chaines de caractères aux caractéristiques identiques.<br/>


## Fonctionnement
Le moteur de recherche effectue une première requête afin de définir s'il existe dans la table 'individus' 
une personne identifiée strictement avec les critères cités plus haut. 

* Si cette recherche est fructueuse et qu'il n'y a qu'un résultat, le moteur récupère l'ensemble des informations liées à la personne 
et détermine si les aides dont la personne a bénéficié sont récupérables (ASG) ou pouvent donner lieu à trop perçu (APA, PCH).<br/>
La règle définie dans le cas où la personne aurait perçue les deux types d'aide est que l'aide récupérable plus importante que le trop perçu.<br/>
Par exemple : une personne ayant perçu de l'APA et de l'ASG, l'application remontera l'ASG.<br/>

* Si cette recherche est fructueuse et qu'il y a plusieurs résultats, 
alors la réponse est redirigée vers le service successions, avec les informations saisies pour la recherche. <br/>

* Si cette recherche revient infructueuse, alors on relance une recherche en élargissant les critères. 
Seule l'année de naissance est conservée pour la recherche.
S'il y a un résultat, il est envoyé aux services successions pour une analyse humaine. 
S'il n'y a aucun résultat, la personne est considérée inconnue et l'information est communiquée au demandeur. <br/>

À la suite de cette recherche, dans le cas où la personne est inconnue, 
le moteur recherche la personne de la même manière au sein de l’entrepôt des demandes en instruction.

___



# Comment sont gérées les réponses aux recherches ?

## Procesus

1.	Un message s’affiche sur l’interface à la suite de la recherche pour indiquer que la recherche a abouti et que l’individu est ou n’est pas connu.


2.	Les courriers PDF de réponse sont simultanément envoyés en pièce-jointe de l'email.

* Individu inconnu : 
un email indique que l’individu n’est pas connu, le courrier de réponse négative est en pièce-jointe. 
Email envoyé au notaire uniquement.

* Individu ayant touché l’APA ou la PCH :
un email indique que l’individu est connu, le courrier de réponse APA-PCH est en pièce-jointe. 
Email envoyé au notaire et au référent administratif de rattachement (pour gestion du potentiel indu).

* Individu ayant touché l’ASG :
un email indique que l’individu est connu, le courrier de réponse  ASG est en pièce-jointe.
Email envoyé au notaire et au gestionnaire des recours (pour gestion de la potentielle créance).

##	Cas ambigus

### Définition
Les cas ambigus apparaissent lorsque les critères de recherche saisis ne permettent pas d’identifier un individu de manière certaine :
* si plusieurs individus correspondent à l’ensemble des critères saisis

* si pour un individu, les jours et mois de naissance varient alors que l’ensemble des critères obligatoires correspondent 


### Processus de réponse
1.	Un message s’affiche sur l’interface à la suite de la recherche pour indiquer que la recherche n’a pas abouti.

2.	un email  est simultanément envoyé au notaire, ainsi qu’au gestionnaire des recours, pour une prise en charge manuelle de la recherche.

___


# Indicateurs

Les indicateurs sont accessibles directement depuis l’application, via l'espace dédié au métier. 


## Statistiques
Les statistiques sont mensuelles et reprennent les :
-	nombre de demandes 
-	nombre de réponses ASG
-	nombre de réponses APA PCH 
-	nombre de réponses négatives
-	nombre de cas litigieux qui demandent une gestion manuelle
-	nombre d'études différentes qui utilisent l'interface

## Liste des recherches

Les services métiers accèdent également à :

-	la liste exhaustive des individus recherchés
-	accompagnée des courriers PDF générés

___


# Comment se connecter à l'application ?

La connexion à l'application se fait par la page d'authentification via identifiant et mot de passe.

## Comment modifier son mot de passe ? 

Une fois connecté à l'application, directement via les informations liées au profil utilisateur.

## Comment générer un nouveau mot de passe ? 

En cliquant sur "J'ai oublié mon mot de passe".<br/> 
Cette action envoie un email sur l'adresse liée à l'identifiant saisi. 
Cet email contient le lien de réinitialisation du mot de passe.

## Comment les comptes utilisateurs sont-ils sécurisés ?
Après 3 tentatives de saisie de mot de passe infructueuses,
le compte utilisateur est verrouillé.
Un email est alors envoyé sur l'adresse liée à l'identifiant saisi.
Cet email contient un code de dévérouillage associé au compte.
___


# Quels sont les differents profils de l'application ?

## Le profil "Étude notariale"
Cet environnement permet aux études notariales du Rhône d'effectuer des recherches 
dans le cadre d'une succession pour les individus ayant vécu dans le département du Rhône.
Il permet également d'accéder à l'historique des recherches effectuées ainsi qu'à la réponse fournie.

## Le profil "Métier"
L'environnement dédié aux équipes métiers permet d'accéder aux recherches que les études notariales ont effectuées. 
Il permet également de consulter les statistiques d'utilisation en temps réel.  

## Le profil "Administrateur"
L'environnement d'administration est uniquement réservé aux administrateurs de l'application. 
Il permet d'accéder à toutes les fonctions de l'application, ainsi qu'aux logs d'import de l'application tiers.
___

