﻿<?php
require_once('notaires_fonctions.php');

sessionCheck();


if(isAdmin()){
  
ob_end_clean();
require_once('vendor/fpdf/fpdf.php');
require_once('vendor/jpgraph/jpgraph.php');
require_once('vendor/jpgraph/jpgraph_bar.php');
require_once('vendor/jpgraph/jpgraph_line.php');

class PDF_MC_Table extends FPDF
{
    var $widths;
    var $aligns;
    
    function SetWidths($w)
    {
        //Tableau des largeurs de colonnes
        $this->widths=$w;
    }
    
    function SetAligns($a)
    {
        //Tableau des alignements de colonnes
        $this->aligns=$a;
    }
    
    function CheckPageBreak($h)
    {
        //Si la hauteur h provoque un d�bordement, saut de page manuel
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }
    
    function NbLines($w,$txt)
    {
        //Calcule le nombre de lignes qu'occupe un MultiCell de largeur w
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
            $s=str_replace("\r",'',$txt);
            $nb=strlen($s);
            if($nb>0 and $s[$nb-1]=="\n")
                $nb--;
                $sep=-1;
                $i=0;
                $j=0;
                $l=0;
                $nl=1;
                while($i<$nb)
                {
                    $c=$s[$i];
                    if($c=="\n")
                    {
                        $i++;
                        $sep=-1;
                        $j=$i;
                        $l=0;
                        $nl++;
                        continue;
                    }
                    if($c==' ')
                        $sep=$i;
                        $l+=$cw[$c];
                        if($l>$wmax)
                        {
                            if($sep==-1)
                            {
                                if($i==$j)
                                    $i++;
                            }
                            else
                                $i=$sep+1;
                                $sep=-1;
                                $j=$i;
                                $l=0;
                                $nl++;
                        }
                        else
                            $i++;
                }
                return $nl;
    }
    
    // En-tête
    function Header1($nom_application,$logo_pdf_departement2)
    {
        $this->Image($logo_pdf_departement2,10,6,30);
        // Police Arial gras 15
        $this->SetFont('Arial','B',18);
        // D?lage ?roite
        $this->Cell(40);
        
        $this->SetFillColor(0,102,178);
        $this->SetTextColor(250);
        
        // Titre
        //$this->Cell(150,20,str_replace("&#039;","'",$titre),0,0,'C',true);
        $this->MultiCell(200,10,utf8_decode("Statistiques d'utilisation de l'application ".$nom_application),0,'C',true);
        //$this->Cell(150,20,str_replace("&#039;","'",$titre),0,0,'C',true);
        // Saut de ligne
        $this->Ln(5);
        $this->SetTextColor(0);
    }
    
    // Pied de page
    function Footer()
    {
        // Positionnement � 1,5 cm du bas
        $this->SetY(-15);
        // Arial italique 8
        $this->SetFont('Arial','I',8);
        // Couleur du texte en gris
        $this->SetTextColor(128);
        // Numéro de page
        $this->Cell(0,10,'Date d\'impression '.date('d/m/Y'),0,0,'L');
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
    }
    
    
    // Tableau coloré
    function FancyTable($header, $data)
    {
        //Calcule la hauteur de la ligne
        $nb=0;
        for($i=0;$i<count($header);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$header[$i]));
            $h=7*$nb;
            //Effectue un saut de page si nécessaire
            $this->CheckPageBreak($h);
            
            // Couleurs, épaisseur du trait et police grasse
            $this->SetFillColor(0,102,178);
            $this->SetTextColor(250);
            $this->SetDrawColor(0,0,0);
            $this->SetLineWidth(.3);
            $this->SetFont('','B');
            // En-t�te
            
            for($i=0;$i<count($header);$i++){
                //$this->Cell($this->widths[$i],7,$header[$i],1,0,'C',true);
                $w=$this->widths[$i];
                //$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
                //Sauve la position courante
                $x=$this->GetX();
                $y=$this->GetY();
                //Dessine le cadre
                $this->Rect($x,$y,$w,$h,"FD");
                //Imprime le texte
                $this->MultiCell($w,7,utf8_decode($header[$i]),0,'C',false);
                
                //Repositionne à droite
                $this->SetXY($x+$w,$y);
                
            }
            $this->Ln($h);
            // Restauration des couleurs et de la police
            $this->SetFillColor(224,235,255);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Données
            $fill = false;
            foreach($data as $row)
            {
                for($i=0;$i<count($header);$i++){
                    if($i==0){$this->SetFont('','B');}else{$this->SetFont('');}
                    $this->Cell($this->widths[$i],6,$row[$i],'LR',0,'C',$fill);
                }
                
                /*$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
                 $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
                 $this->Cell($w[2],6,number_format($row[2],0,',',' '),'LR',0,'R',$fill);
                 $this->Cell($w[3],6,number_format($row[3],0,',',' '),'LR',0,'R',$fill);*/
                $this->Ln();
                $fill = !$fill;
            }
            // Trait de terminaison
            $this->Cell(array_sum($this->widths),0,'','T');
    }
    
}

//$pdf = new PDF();
$pdf=new PDF_MC_Table();
// Chargement des donn�es

$pdf->AddPage('L');
$pdf->Header1($nom_application,$logo_pdf_departement2);
$pdf->SetFont('Arial','',10);

$header = array('', 'Nb demandes', 'Nb réponses totales', 'Nb réponses récupèrables', 'Nb réponses indus probables', 'Nb réponses cas ambigus', 'Nb réponses individus inconnus', 'Nb connexions', 'Nb d\'utilisateurs différents');

$data=array();
//Recup
$data1y=array();
//Indus
$data2y=array();
//Ambigus
$data3y=array();
//Demandes
$data4y=array();
//Inconnus
$data5y=array();
//Connexions
$data6y=array();

$months = array();

$pdf->SetWidths(array(35, 30, 30, 30,30,30,30,30,30));


$startDate=$_SESSION['stats_year_startNotaire'].'-'.($_SESSION['stats_month_startNotaire']-1).'-01';
$endDate=$_SESSION['stats_year_endNotaire'].'-'.$_SESSION['stats_month_endNotaire'].'-01';

$start = $startDate;
$i = 1;
if (strtotime($startDate) < strtotime($endDate)) {
    while (strtotime($start) < strtotime($endDate)) {
        $start = date('Y-m-d', strtotime($startDate . '+' . $i . ' month'));
        $date=explode('-',$start);
        $mois_actu = strftime("%B-%y ", mktime(0, 0, 0, $date[1]+1, 0, $date[0]));
        $data[]=array($mois_actu,staque($date[1],$date[0]),stats_rep($date[1],$date[0]),starepnat($date[1] ,"1",$date[0]),starepnat($date[1] ,"2",$date[0]),starepnat($date[1],"4",$date[0]),starepnat($date[1],"3",$date[0]),status($date[1],$date[0]),staudif($date[1],$date[0]));
        $months[]=$mois_actu;
        //echo $mois_actu.'<br/>';
        //Recup
        $data1y[]=starepnat($date[1] ,"1",$date[0]);
        //Indus
        $data2y[]=starepnat($date[1],"2",$date[0]);
        //Ambigus
        $data3y[]=starepnat($date[1],"4",$date[0]);
        //Demandes
        $data4y[]=staque($date[1],$date[0]);
        //Inconnus
        $data5y[]=starepnat($date[1],"3",$date[0]);
        //Connexions
        $data6y[]=status($date[1],$date[0]);
        
        $i++;
    }
}

$pdf->FancyTable($header,$data);

$count=count($data1y);
$nb_t=ceil(($count)/12);

//echo $count."<br>";
//echo $nb_t."<br>";

$nb_i=(($count)/12);
$nb_m=(($count)%12);

//$pdf->AddPage('L');
//$pdf->Cell(100,6,count($data1y).' - '.$nb_t.' - '.$nb_i.' - '.$nb_m,'LR',0,'C',false);


for($o=1;$o<=$nb_t;$o++){
    
    $pdf->AddPage('L');
    $pdf->Header1($nom_application,$logo_pdf_departement2);
    $pdf->SetFont('Arial','',10);
    
    // Initialisation du graphique
    $graph = new Graph(1080,700,'auto');
    $graph->SetScale("textlin");
    $graph->SetColor("#F2F2F2");
    $graph->SetY2Scale("lin",0,1050);
    $graph->SetY2OrderBack(false);
    
    $graph->SetMargin(35,50,20,5);
    
    $theme_class = new UniversalTheme;
    $graph->SetTheme($theme_class);
    
    //$graph->yaxis->SetTickPositions(array(0,150,300,450,600,750,900,1050,1200,1350,1500,1650), array(0,100,200,300,400,500,600,700,800,900,1000,1100));
    //$graph->y2axis->SetTickPositions(array(0,10,20,30,40,50,60,70,80,90,100,110,120,130,140));
    
    $graph->SetBox(false);
    
    $graph->ygrid->SetFill(false);
    $graph->xaxis->SetTickLabels(array('A','B','C','D'));
    $graph->yaxis->HideLine(false);
    $graph->yaxis->HideTicks(false,false);
    
    
    if($o==1)
    {
        $o1a=0;
        if($count>=12)
        {
            $o2a=12;
        }
        else
        {
            $o2a=$count;
            //if($o2a==0){$o2a=1;}
            /*if($nb_t==1 || $nb_t==2){
             $o1a=0;
             }*/
        }
    }
    else
    {
        $o1a=($o-1)*12;
        $l=($o1a+12);
        if($count>=$l)
        {
            $o2a=$l;
        }
        else
        {
            $o2a=$count;
        }
    }
    
    
    $months1=array();
    $data1y1=array();
    $data2y1=array();
    $data3y1=array();
    $data4y1=array();
    $data5y1=array();
    $data6y1=array();
    
    //echo $o2a."<br>";
    //echo $o1a."<br>";
    
    for($o1=$o1a;$o1<$o2a;$o1++){
        //echo $o1;
        $months1[]=$months[$o1];
        //echo $o.')'.$months[$o1].','.$data1y[$o1].','.$data2y[$o1].','.$data3y[$o1].','.$data4y[$o1].','.$data5y[$o1].','.$data6y[$o1].'<br/>';
        $data1y1[]=$data1y[$o1];
        $data2y1[]=$data2y[$o1];
        $data3y1[]=$data3y[$o1];
        $data4y1[]=$data4y[$o1];
        $data5y1[]=$data5y[$o1];
        $data6y1[]=$data6y[$o1];
        
    }
    
    // Setup month as labels on the X-axis
    //$graph->xaxis->SetTickLabels($months);
    $graph->xaxis->SetTickLabels($months1);
    
    // Create the bar plots
    // Récupérables
    //$b1plot = new BarPlot($data1y);
    $b1plot = new BarPlot($data1y1);
    //Indus Probables
    //$b2plot = new BarPlot($data2y);
    $b2plot = new BarPlot($data2y1);
    //Ambigus
    //$b3plot = new BarPlot($data3y);
    $b3plot = new BarPlot($data3y1);
    //Demandes
    //$b4plot = new BarPlot($data4y);
    $b4plot = new BarPlot($data4y1);
    //$b5plot = new BarPlot($data5y);
    $b5plot = new BarPlot($data5y1);
    
    //$lplot = new LinePlot($data6y);
    $lplot = new LinePlot($data6y1);
    
    //echo "ee";
    // Create the grouped bar plot
    // $gbbplot = new AccBarPlot(array($b3plot,$b4plot,$b5plot));
    $gbplot = new AccBarPlot(array($b1plot,$b2plot,$b3plot,$b5plot));
    $gbbplot = new GroupBarPlot(array($b4plot,$gbplot));
    
    // ...and add it to the graPH
    $graph->Add($gbbplot);
    $graph->AddY2($lplot);
    
    // Récupérables
    $b1plot->SetColor("#1D6F99");
    $b1plot->SetFillColor("#1D6F99");
    $b1plot->SetLegend("Récupérables");
    
    //Indus Probables
    $b2plot->SetColor("#FFBEB5");
    $b2plot->SetFillColor("#FFBEB5");
    $b2plot->SetLegend("Indus Probables");
    //Ambigus
    $b3plot->SetColor("#CC646C");
    $b3plot->SetFillColor("#CC646C");
    $b3plot->SetLegend("Ambigus");
    //Demandes
    $b4plot->SetColor("#d2bf64");
    $b4plot->SetFillColor("#d2bf64");
    $b4plot->SetLegend("Demandes");
    $b4plot->value->SetFormat('%d');
    $b4plot->value->Show();
    $b4plot->value->SetColor('#d2bf64');
    //Inconnus
    $b5plot->SetColor("#64A8CC");
    $b5plot->SetFillColor("#64A8CC");
    $b5plot->SetLegend("Inconnus");
    //Connexions
    $lplot->SetBarCenter();
    $lplot->SetColor("black");
    $lplot->SetLegend("Connexions");
    $lplot->mark->SetType(MARK_SQUARE,'',1.0);
    $lplot->mark->SetWeight(2);
    $lplot->mark->SetWidth(2);
    $lplot->mark->setColor("black");
    $lplot->mark->setFillColor("black");
    $lplot->value->SetFormat('%d');
    $lplot->value->Show();
    $lplot->value->SetColor('black');
    
    $graph->legend->SetFrameWeight(1);
    $graph->legend->SetColumns(6);
    $graph->legend->SetColor('#9b9b9b','black');
    
    $band = new PlotBand(VERTICAL,BAND_RDIAG,11,"max",'khaki4');
    $band->ShowFrame(true);
    $band->SetOrder(DEPTH_BACK);
    $graph->Add($band);
    
    //$graph->title->Set("Progression des demandes");
    
    $nom_image="pdf/".random(10).'.png';
    $graph->Stroke($nom_image);
    $pdf->Image($nom_image,10,30,'','','PNG');
    unlink($nom_image);
}

$pdf->AliasNbPages();
$pdf->Output("Stats notaire.pdf",'D');
//$pdf->Output();
}else{
	header ('Location: index.php');
}
?>