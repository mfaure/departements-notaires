﻿
<div id="encart_aide">
  <div id="texte_aide">
    <table style='border: 1px solid black; background-color: #9b9b9b;'>
      <tr>
        <td><div id="cercle">
            <p id="warning">&nbsp;!</p>
          </div></td>
        <td><b>Avant d'effectuer une recherche, vous devez être en possession de l'acte de décès de la personne dont
            vous êtes en charge de la succession.</b></td>
      </tr>
    </table>
    <br />

    <h1>Pour vous connecter</h1>
    <br />
    <h2>Accès à l'application</h2>
    <p class="aide" align="justify">
      L'adresse web de {{config.nom_application}} est : {{config.adresse_url_appli}}
      <br />
      L'application est également accessible via le site web du {{config.nom_departement_long}} : {{config.site_departement}}
    </p>

    <p class="nota">
      <i>nota</i> : pour un confort optimal de navigation, nous vous recommandons d'utiliser les dernières versions des
      navigateurs du marché.
    </p>
    <br/>
    
    <h2>Comptes</h2>
    <p class="aide" align="justify">
      Chaque étude notariale du {{config.nom_departement_commun}} dispose d'un propre compte de connexion. 
      Ce compte est générique pour l'ensemble des associés potentiels d'une étude.<br /> A chaque compte est associé :
    </p>
    <ul>
      <li>un nom
      
      <li>un identifiant (CERPCEN)
      
      <li>un mot de passe
      
      <li>une adresse de messagerie
    
    </ul>
    <p class="aide" align="justify">L'adresse de messagerie associée à votre compte correspond à l'adresse générique de
      votre étude, diffusée par la chambre des notaires du {{config.nom_departement_commun}}.</p>

    <h2>Connexion</h2>
    <p class="aide" align="justify">Les éléments nécessaires à votre connexion sont votre identifiant et votre mot de
      passe.</p>
    <ul>
      <li>L'identifiant de votre étude correspond à votre code CRPCEN (ex : 69103).</li>
      <li>Le mot de passe de première connexion vous a été envoyé par mail à l'adresse de messagerie associée à votre
        compte. Il vous sera demandé de le personnaliser dès votre première connexion.</li>
    </ul>



    <h1>Pour effectuer une recherche</h1>
    <br/>
    <p class=nota>
      <i>nota</i> : les dates renseignées devront être comprises entre le 1er janvier 1900 et la date de la veille de
      votre saisie.
    </p>
    <br/>
    <h2>Renseignements sur l'acte de décès</h2>
    <ul>
      <li>cocher la case attestant que vous détenez bien l'acte de décès (saisie obligatoire)</li>
      <li>renseigner la date du décès (sous le format jjmmaaaa, sans les séparer par des /) (saisie obligatoire)</li>
      <li>renseigner le lieu du décès (saisie obligatoire)</li>
      <li>renseigner la date à laquelle l'acte de décès a été dressé (sous le format jjmmaaaa, sans les séparer par des
        /) (saisie obligatoire)</li>
      <li>renseigner le champ destinataire (facultatif). Les informations saisies dans ce champ apparaîtront dans
        l'objet du mail relatif à votre recherche, et vous permettront d'identifier le collaborateur en charge de la
        succession.</li>
    </ul>

    <h2>Renseignements sur la personne décédée dont vous êtes en charge de la succession</h2>
    <ul>
      <li>renseigner son premier prénom figurant sur l'état civil (que ce soit un prénom simple ou composé) (saisie
        obligatoire)</li>
      <li>renseigner le cas échéant son deuxième prénom figurant sur l'état civil (saisie non obligatoire)</li>
      <li>les autres prénoms peuvent être ajoutés en cliquant sur le '+' ; ils devront être séparés par des virgules</li>
      <li>renseigner son nom d'usage (nom patronymique ou bien nom d'époux) (saisie obligatoire)</li>
      <li>renseigner le cas échéant son nom figurant sur l'état civil (nom patronymique si le nom d'usage est le nom
        d'époux) (saisie non obligatoire)</li>
      <li>renseigner sa date de naissance (sous le format jjmmaaaa, sans les séparer par des /) (saisie obligatoire)</li>
    </ul>
    <p class="nota">
      <i>nota</i> : pour les services du département, la personne est connue sous son nom d'usage, celui qu'elle utilise
      dans ses démarches quotidiennes (souvent, le nom d'épouse pour les femmes mariées). Aussi c'est celui qui doit
      être indiqué en premier. Le nom d'état civil ne doit être complété que si le nom d'usage est différent du nom de
      naissance.
    </p>
    <br/>

    <h1>La recherche :</h1>
    <br/>

    <p class="aide" align="justify">
      Une fois que vous avez appuyé sur le bouton "recherche", une page s'ouvre pour vous informer que la réponse,
      quant à votre interrogation au sujet d'une éventuelle créance du département, vous a été adressée par message
      électronique. <br /> <br />Le message électronique et son courrier en pièce-jointe vous informeront si la personne
      a bénéficié ou non d'une aide sociale récupérable du département. <br /> <br />La réponse est délivrée par
      l'interface du département sous réserve de l'exactitude des éléments renseignés. <br />
    </p>
    <p class="nota">
      <i>nota</i> : il est probable qu'il ne soit pas possible d'établir avec certitude l'identité de la personne. C'est
      le seul cas où vous n'aurez pas de réponse immédiate. En effet, dans ce cas, les services feront une recherche et
      vous adresseront leur réponse dans les meilleurs délais.
    </p>
    <br />

    <h1>Le Support :</h1>
    <br/>

    <p class="aide" align="justify">
      Pour toute difficulté d'ordre technique ou fonctionnelle, vous pouvez adresser un message électronique à : 
      <a class="mail" href="mailto:{{config.mail_gestion}}">Service successions</a> 
      <br /> <br />
      L'application est accessible à toutes les études notariales du {{config.nom_departement_commun}} 24h/24 et 7j/7, 
      le support est assuré uniquement durant les journées ouvrées. Le support de l'application se fait par mail à 
      l'adresse {{config.mail_gestion}}.</p>
  </div>
</div>  